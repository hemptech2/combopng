#!/usr/bin/env python

import os, shutil, errno

#------------------------------------------------------------
#---------- Getting the users starting folder ---------------
#------------------------------------------------------------
    
def setDir(xDir):

    # Condtioning variable	
	UserResponse = True
    
	while UserResponse:
		
		# Getting the user's input for where there files are stored
		xDir = input ("Enter the folder path you want ==> ");
		print("\nYou entered ===> %s" % xDir)
				
		print("\nIs that correct? ")	     
		UserAnswer = raw_input("Please type [y/n]...==> ")

		if UserAnswer != 'y':
				UserResponse = True
				os.system('clear')
				WelScreen()                
		else:
				UserResponse = False
				os.chdir(xDir)
				os.system('clear')
				
	return xDir

#-------------------------------------------------------------------
#----- Creating a combo folder for all files to be stored ----------    
#-------------------------------------------------------------------

def makeDir(xDir):
	
	# Getting the Working Folder and also setting up a folder
	# prototype to store all of the files
	createFolder = '/xComboFiles'
	FulPath= xDir + createFolder

	#Will attempt to make the ComboFiles folder inside Working Folder
	#If it doesn't already exist.
	try:
		os.makedirs(FulPath)
		print ("\nxComboFiles folder Created")
	except OSError as exception:
		
		if exception.errno != errno.EEXIST:
			raise
		else:
			print ("\nDirectory %s already Exist." % FulPath)
			print ""

	# Getting the Working Folder and also setting up a folder
	# prototype to store all of the files
	createFolder = "/xTemp"
	FulPath= xDir + createFolder

	#Will attempt to make the ComboFiles folder inside Working Folder
	#If it doesn't already exist.
	try:
		os.makedirs(FulPath)
		print ("\nxTemp folder Created")

	except OSError as exception:
		
		if exception.errno != errno.EEXIST:
			raise
		else:
			print ("\nDirectory %s already Exist." % FulPath)
			print ("\n")
			
#-------------------------------------------------------------------
#-------- Creating a user defined Subfolder priority list ----------    
#-------------------------------------------------------------------

def BuildList(xDir,TempList, Cnt_var):
        
	# Defining local list variable
	TempList = MstrList = []
	Cnt_var = ScrnNum = iCnt = 0
	TmpStr = TmpStr2 = nStr = ''
	
	# getting the subfolder names in a list format
	# and finding the total number of subfolders
	SubDirs= next(os.walk(xDir))[1]
	myListCnt = len(SubDirs)
	Cnt_var = myListCnt - 2
	ScrnNum += 1
		
	TitleBump(xDir, ScrnNum)
	for iCnt in range (0, myListCnt):
		nStr = str(SubDirs[iCnt])
		if nStr == "xComboFiles":
			#Don't show folder in list
			iCint = iCnt
		elif nStr == "xTemp":
			#Don't show folder in list
			iCint = iCnt
		else:
			print nStr
	
	print ("\n[Remember case sensitive]")	    		
	
	for xSeq in range (0, Cnt_var):	

		FName = raw_input ("Type the folder sequence you want ==> ")
		for a in range (0, Cnt_var):
			
			# Making sure the user entered the name in the correct
			# case format.
			if SubDirs[a].lower() == FName.lower():
				FName = SubDirs[a]
				
		TmpStr2 = xDir + '/' + FName
		MstrList.append (TmpStr2)
		
	ScrnNum += 1	
	TitleBump(xDir, ScrnNum)
	
	for j in range (0, Cnt_var):
		print MstrList[j].split("/")[5]
		
	Cnt1 = len(MstrList)
	ScrnNum += 1		
	TitleBump(xDir, ScrnNum)	
	SqNam = raw_input("Enter (y) for 00name.xx or (n) for 0000.xx ==> ")
	
	if SqNam != 'y':
		ScrnNum += 1
		TitleBump(xDir, ScrnNum)
		Ext= raw_input("Type your extention and press enter ==> ")
	else:
		Ext = ''
		
	#Writing the final files to the ComboFiles folder			
	WriteFiles(xDir, Cnt1, MstrList, SqNam, Ext)	
	
#-------------------------------------------------------------------
#------ Copying & renaming files into the ComboFiles folder --------
#-------------------------------------------------------------------
def WriteFiles(eDir, xCnt1, MstrList1, xSqNam, xExt):
	
	# Defining Local variable
	TempCnt = 0	
	
	# Going through all the subfolder in the list
	for x in range (0, xCnt1):
		# Moving to selected folder
		os.chdir(MstrList1[x])	

	    # getting the subfolder names in a list format
		# and finding the total number of subfolders
		SubDirs= next(os.walk(eDir))[1]
		myListCnt = len(SubDirs)
				
		# Determining how many files are in selected folder
		file_List = os.walk(MstrList1[x]).next()[2]
		myFileCnt = len(file_List)
				
		# Copying the files from the subfolder and into the
		# ComboFiles Folder
		for xFiles in range (0, myFileCnt):
		
			prevName = MstrList1[x] + '/' + file_List[xFiles]
			# xTemp
			newName = eDir + '/xTemp/' + file_List[xFiles] 
			shutil.copy(prevName,newName)
	
			# Changing to the xTemp folder
			os.chdir(eDir + "/xTemp")
		
			# Using a counter to number and rename the 
			# new files in the temp folder
			TempCnt += 1
			tmpString = str(TempCnt)
			if xSqNam == 'y':
				tmpString = tmpString.zfill(2) + file_List[xFiles]
			else:	
				tmpString = tmpString.zfill(4) + xExt
			
			os.rename(newName,tmpString)
			finalName = eDir + "/xTemp/" + tmpString
			finalPath = eDir + "/xComboFiles/" + tmpString
			
			# xComboFiles folder
			os.rename(tmpString, finalPath)
				
#------------------------------------------------------------
#------------- Main Title Welcoming Message -----------------
#------------------------------------------------------------
def WelScreen():
	os.system('clear') 	
	print("*********************************************************")
	print("* ==========================================            *")
	print("*   Multi-File renaming and transfer Script             *")
	print("* ==========================================            *")
	print("* This script takes the user's files in subfolders      *")
	print("* and renames them in a sequencial format. That is      *")
	print("* placed in (ComboFiles) folder in your selected        *")
	print("* directory.                                            *")
	print("*                          coded by: Cornelius Hemphill *")
	print("*********************************************************")	

#------------------------------------------------------------
#------------------- Title Bump Message ---------------------
#------------------------------------------------------------
def TitleBump(cDir, TitleNum):
	
	# TitleNum will determine which Title gets displayed
	if TitleNum == 1:
		lcnt = len(cDir.split("/")) - 1
		print("******************************************")
		print(" The %s folder is selected" % cDir.split("/")[lcnt])
		print("******************************************")
		print ("\nHere's the subfolders")
		print ("======================")
		
	elif TitleNum == 2:
		os.system('clear') 
		print("=============================")
		print("= This is your folder order =")
		print("=============================")
	
	elif TitleNum == 3:
		print "\n\nDo you want to keep the original file name"
		print "but adds 00's to the beginning?"	
	
	elif TitleNum == 4:
		print ("\nWhat's the ext for seq numbering. Exp: (.ogg, .txt)")
		
	elif TitleNum == 5:
		os.system('clear') 
		print("******************************************************")
		print("* Thanks for using the program! Hope it helped. :-D  *")
		print("*                                                    *")
		print("* Scripting Resource acknowledgments                 *")
		print("* ===================================                *")
		print("* ---------------------                              *")
		print("* | Youtube tutorials |                              *")
		print("* ---------------------                              *")
		print("* sentdex                                            *")
		print("* Corey Schafer                                      *")
		print("* -----------                                        *")		
		print("* | The Web |                                        *")	
		print("* -----------                                        *")
		print("* StackOverFlow.com & others I don't remember.  :-(  *")							
		print("******************************************************")
				
#-------------------------------------------------------------------
#---------------------- Main program Function ----------------------    
#-------------------------------------------------------------------

def myMain():
	
	#------------------------------
	# Local variables to main but 
	# will be utilized like they 
	# are global variables
	#------------------------------
	b_var = PassedAns = DirPath = ''
	myCnt = 0
	c_var = []
	
	mIsRunning = True, 
	
	WelScreen()
	
	while mIsRunning:
		# Getting the user's folder path
		DirPath = setDir(b_var)
		os.system('clear')
		
		# Making the ComboFiles Folder
		makeDir(DirPath)
		
        # Building the user's folder list
		BuildList(DirPath, c_var, myCnt)
		
		print("\nStart a new process?")
		PassedAns = raw_input("Type (y) or (n) then press enter ==> ")
		
		if PassedAns != 'y':
			TitleBump(DirPath, 5)
			mIsRunning = False
		else:
			WelScreen()
			

#------- "It Starts..." -------
# Entry Point for program
myMain()