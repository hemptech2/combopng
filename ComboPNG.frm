'*************************************************
'   This is a vb6 program that takes a users
'   selected folder and subfolders and copies
'   them to a combo folder. The files are renamed
'   to be in a sequential pattern
'   exp: 0001.png, 0002.png, etc
'**************************************************

Option Explicit

Dim CmbChange, SubFolder As Variant
Dim xcnt, MasterList() As Variant
Dim isChecked As Boolean


Private Sub ckPathEnable_Click()
On Error GoTo errhandle:

'Checking to see what state the check box is in.
'then show display data in the label

If ckPathEnable.Caption = "Path Enabled" And isChecked = False Then
    isChecked = True
    CmbChange = Dir1.Path
    lblDirPath.Caption = CmbChange

ElseIf ckPathEnable.Caption = "Path Enabled" And isChecked = True Then
    isChecked = False
    ckPathEnable.Caption = "Path Disabled"
    ClearData
           
Else
    isChecked = True
    CmbChange = Dir1.Path
    ckPathEnable.Caption = "Path Enabled"
    lblDirPath.Caption = CmbChange
    MkDir Dir1.Path + "\xComboFiles"
    Dir2 = Dir1 + "\xComboFiles"
End If
Exit Sub

errhandle:
 'error trying to make the xComboFiles folder. It already exists
 Dir2 = Dir1 + "\xComboFiles"
End Sub

Private Sub cmdClearList_Click()
ClearData
End Sub

Private Sub cmdMoveFile_Click()
Dim TempStr, TempStr2, TempStr3, TempStr4, xVar, xExt As String
Dim i, j, MasterCnt As Integer
Dim ExCmd, MyAnswer As Variant

Dim fso As New FileSystemObject

j = 0

If isChecked = True Then
    MasterCnt = List1.ListCount
    lblMoveMess.Caption = "Moving" + Str(MasterCnt) + " Files"
    TempStr2 = Dir1.Path + "\xComboFiles"
    
    'File Names in Selected Folder
    List1.ListIndex = i
    'Getting the Current File extention
    xExt = Right$(List1.Text, 4)
    MyAnswer = MsgBox("This is the current file extention " & xExt & ". Is this Correct?", _
    vbYesNo, "File Extention")
    
    If MyAnswer = vbYes Then
       xExt = xExt
    Else
       xExt = InputBox("Enter the extention", "File Extention")
    End If
    
    For i = 0 To MasterCnt - 1
        
        List1.ListIndex = i   'File Names in Selected Folder
        List2.ListIndex = i   'Total Path string in current folder
        
        TempStr = List2.Text
        j = j + 1
        
        'Padding the current number with beginning zeros
        xVar = Right("0000" & j, 5)
        
        'Getting the Current File extention
        xExt = Right$(List1.Text, 4)
        
        'Dir path plus ComboFiles/FileName
        TempStr3 = TempStr2 + "\" + List1.Text
        
        'New Number FileName
        TempStr4 = xVar + xExt
        
        'Using a batch file to copy to, and rename files in xComboFiles Folder
        ExCmd = App.Path + "\ComboFiles.bat " & TempStr & _
        " " & TempStr2 & " " & TempStr3 & " " & TempStr4
        
        Shell ExCmd   'Executing the batch file
        
    Next i
    
    'Showing the files in xComboFiles Folder
    Dir2_Change
    MsgBox ("Done! Copied " & MasterCnt & " Files to xComboFiles Folder")
    
End If
End Sub

Private Sub cmdSubSelect_Click()
If isChecked = True Then
 lstUserFolder.AddItem (SubFolder)
 LoadData
 Dir1 = CmbChange
End If

End Sub

Private Sub Dir1_Change()

If isChecked = True Then
    lblDirPath.Caption = CmbChange
    TrimData
Else
    lblDirPath.Caption = ""
End If
End Sub

Private Function LoadData()
'Loading all the song files into
'the listbox
Dim fso, fc, f1, file1, f, s

Set fso = CreateObject("Scripting.FileSystemObject")
Set f = fso.GetFolder(Dir1.Path)
Set fc = f.Files

For Each f1 In fc
s = fso.GetFileName(f1.Name)
List1.AddItem (s)
List2.AddItem (Dir1.Path + "\" + s)
Next
End Function
Private Function LoadData2()
'Loading all the song files into
'the listbox
Dim fso, fc, f1, file1, f, s

Set fso = CreateObject("Scripting.FileSystemObject")
Set f = fso.GetFolder(Dir2)
Set fc = f.Files

For Each f1 In fc
s = fso.GetFileName(f1.Name)
lstComboFiles.AddItem (s)
Next
End Function

Private Sub Dir2_Change()
'Showing all the files in xComboFiles folder
LoadData2
End Sub

Private Sub Form_Load()
Dir1 = "c:\"

End Sub

Function TrimData()

Dim stringlength As Integer
Dim strLength As Integer
Dim TempStrNum As Integer
Dim x As Integer


If CmbChange <> Dir1.Path Then

   
    'Getting the number of the string
    stringlength = Len(CmbChange)
    TempStrNum = Len(Dir1.Path)
    
    'Making sure all 9 character are being
    'subtracted
    strLength = (TempStrNum - 1) - stringlength

    'Getting rid of the "dir1:\music\" out of
    'the string and storing the Catagory Name
    'in the variable and textbox
    If strLength <= 0 Then
       'Don 't show anything and clear the users path.
       'They've gone too far back
       strLength = strLength
       'ckPathEnable_Click
       ckPathEnable.Value = 0
    Else
        SubFolder = Right(Dir1.Path, strLength)
        
    End If
End If
End Function

Function ClearData()
If isChecked = False Then
    lblDirPath.Caption = ""
End If
lstUserFolder.Clear
List1.Clear
List2.Clear
lblMoveMess = ""
lstComboFiles.Clear

End Function